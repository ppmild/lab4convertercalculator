package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener{

	float intRate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSetting);
		b2.setOnClickListener(this);
		
		TextView iRate = (TextView)findViewById(R.id.irate);
		intRate = Float.parseFloat(iRate.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.button1) {
			EditText etmoney = (EditText)findViewById(R.id.editText1);
			EditText etyear = (EditText)findViewById(R.id.year);
			TextView tvres = (TextView)findViewById(R.id.result);
			String res = "";
			try {
				float money = Float.parseFloat(etmoney.getText().toString());
				float year = Float.parseFloat(etyear.getText().toString());
				float amount = (float) (money * Math.pow((1 + (intRate/100)), year));
				res = String.format(Locale.getDefault(), "%.2f", amount);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			tvres.setText(res);
		}
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("intRate", intRate);
			startActivityForResult(i, 9999); 
			// 9999 is a request code. It is a user-defined unique integer.
			// It is used to identify the return value.
			// want to return new exchange rate
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			intRate = data.getFloatExtra("intRate", 10.0f);
			TextView iRate = (TextView)findViewById(R.id.irate);
			iRate.setText(String.format(Locale.getDefault(), "%.2f", intRate));
		}
	}

}
